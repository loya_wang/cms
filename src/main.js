import Vue from 'vue'
import App from './App.vue'
import '@/components/index.js'
import router from './router/index.js'
import store from './store/index.js'

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  if (localStorage.getItem('token')) {
    if (to.path == '/login') {
      next(from.fullPath)
    } else {
      let length = store.state.login.roles.length
      if (length == 0) {
        store.dispatch('FETCH_USERINFO').then(res => {
          next({ path: to.path })
        })
        return
      }
      next()
    }
  } else {
    if (
      to.matched.length > 0 &&
      !to.matched.some(record => record.meta.requiresAuth)
    ) {
      next()
    } else {
      next({ path: '/login' })
    }
  }
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
