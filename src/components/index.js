import Vue from 'vue'
import { Checkbox, CheckboxGroup, Row, Col, Tree } from 'element-ui'
const componentsList = [Checkbox, CheckboxGroup, Row, Col, Tree]
componentsList.map(item => {
  Vue.component(item.name, item)
})
