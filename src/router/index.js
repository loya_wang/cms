import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/home.vue'
// route level code-splitting
// this generates a separate chunk (about.[hash].js) for this route
// which is lazy-loaded when the route is visited.
const About = () => import(/* webpackChunkName: "about" */ '@/views/about.vue')
const Learn = () =>
  import(/* webpackChunkName: "learn" */ '@/views/learn/index.vue')
const PromiseTest = () =>
  import(/* webpackChunkName: "learn" */ '@/views/learn/promise.vue')
const CheckBox = () =>
  import(/* webpackChunkName: "learn" */ '@/views/learn/checkBox.vue')

const Login = () =>
  import(/* webpackChunkName: "login" */ '@/views/login/login.vue')

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
  ],
})
export const DynamicRoutes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/learn',
    name: 'learn',
    redirect: '/learn/promisetest',
    component: Learn,
    meta: {
      requiresAuth: true,
    },
    children: [
      {
        path: 'promisetest',
        name: 'promiseTest',
        component: PromiseTest,
      },
      {
        path: 'checkbox',
        name: 'checkBox',
        component: CheckBox,
      },
    ],
  },
]
