export default function recursionRouter(userRoutes = [], localRoutes = []) {
  var realRoutes = []
  userRoutes.forEach(x => {
    localRoutes.forEach(y => {
      if (y.name === x.name) {
        if (x.children && x.children.length > 0) {
          y.children = recursionRouter(x.children, y.children)
        }
        realRoutes.push(y)
      }
    })
  })

  return realRoutes
}
